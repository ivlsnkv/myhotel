﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class TypesRooms
    {
        public virtual int idtyperoom { get; set; }
        public virtual int idcategoryroom { get; set; }
        public virtual int idclassroom { get; set; }
        public virtual decimal costnight { get; set; }
        public virtual string picture { get; set; }
        public virtual string description { get; set; }
    }
}