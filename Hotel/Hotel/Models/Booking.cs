﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{

    public class Booking
    {
        
        public virtual int idbooking {  get; set;  }
        public virtual int idclient { get; set; }
        public virtual int idcategoryroom { get; set; }
        public virtual DateTime arrivaldate { get; set; }
        public virtual DateTime departuredate { get; set; }
        public virtual int numberguests { get; set; }
       
    }
}