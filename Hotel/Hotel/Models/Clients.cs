﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Hotel.Models
{
    public class Clients
    {
        public virtual int idclient { get; set; }
        public virtual string lastname { get; set; }
        public virtual string firstname { get; set; }
        public virtual string patronomic { get; set; }
        public virtual string passport { get; set; }
        public virtual string city { get; set; }        
        public virtual string address { get; set; }
        public virtual string phone { get; set; }
        public virtual string email { get; set; }


    }
}