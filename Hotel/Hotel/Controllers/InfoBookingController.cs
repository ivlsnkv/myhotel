﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Hotel.Models;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Hotel.Controllers
{
    public class InfoBookingController : Controller
    {       

        public Configuration myconf1;
        public ISessionFactory myISF1;
        public ISession myIS1;

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult InfoBooking()
        {
            myconf1 = new Configuration();
            myconf1.Configure();
            myISF1 = myconf1.BuildSessionFactory();
            myIS1 = myISF1.OpenSession();

            IList<InfoBooking> cat = myIS1.CreateCriteria(typeof(InfoBooking)).List<InfoBooking>();

            IList<Clients> cat1 = myIS1.CreateCriteria(typeof(Clients)).List<Clients>();
            IList<Booking> cat2 = myIS1.CreateCriteria(typeof(Booking)).List<Booking>();
            IList<TypesRooms> cat3 = myIS1.CreateCriteria(typeof(TypesRooms)).List<TypesRooms>();
            IList<Classes> cat5 = myIS1.CreateCriteria(typeof(Classes)).List<Classes>();
            IList<Categories> cat4 = myIS1.CreateCriteria(typeof(Categories)).List<Categories>();


            String sql = "select Clients.idclient, Clients.lastname, Clients.firstname, Clients.patronomic, Clients.passport, Clients.city, Clients.address, Clients.phone, Clients.email, Booking.idcategoryroom,Booking.numberguests,TypesRooms.idtyperoom from (Clients inner join Booking on Clients.idclient=Booking.idclient) inner join TypesRooms on TypesRooms.idtyperoom=Booking.idcategoryroom";

            IQuery logged =myIS1.CreateSQLQuery(sql);

            var result = new JsonResult()

            {
             
                Data = new
                {
                    page = 0,
                    total = 1,

                    records = 2,

                    rows = (
                
                from co in cat1
                join dist in cat2 on co.idclient equals dist.idclient
                join co1 in cat3 on dist.idcategoryroom equals co1.idtyperoom
                join co2 in cat4 on co1.idcategoryroom equals co2.idcategoryroom
                join co3 in cat5 on co1.idclassroom equals co3.idclassroom
                select new
                {
                    id = co.idclient,//.ToString().Trim(),
                    cell = new string[]
                    {
                        co.idclient.ToString(),
                        co.lastname,//.ToString(),//.Trim(),
                        co.firstname,
                        co.patronomic,
                        co.passport,
                        co.city,
                        co.address,
                        co.phone,
                        co.email,
                       co2.namecategory,//.Trim()                        
                       co3.nameclass,
                        dist.numberguests.ToString()
                    }

                }
                
                ).ToArray(),

                }

            };

            return result;

        }
    }
}
