﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using Hotel.Models;
using Hotel.Controllers;
using NHibernate.Mapping.ByCode;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;


namespace Hotel.Controllers
{
    public class CategoriesController : Controller
    {

        public  Configuration myconf;
        public ISessionFactory myISF;
        public  ISession myIS;

        public ActionResult Index()
        {
            return View();
        }

        public JsonResult EmployeeData()
        {
            myconf = new Configuration();
            myconf.Configure();
            myISF = myconf.BuildSessionFactory();
            myIS = myISF.OpenSession();

            IList<Categories> cat = myIS.CreateCriteria(typeof(Categories)).List<Categories>();

            var result = new JsonResult()

            {

                Data = new
                {
                    page = 0,
                    total = 1,

                    records = 6,

                    rows = (
                from co in cat
                select new
                {
                    id = co.idcategoryroom,
                    cell = new string[]
                    {
                        co.idcategoryroom.ToString(),
                        co.namecategory
                    }

                }).ToArray()

                }

            };

            return result;
           
        }





        public ActionResult DeleteUser(int id)
        {
            try
            {
            myconf = new Configuration();
            myconf.Configure();
            myISF = myconf.BuildSessionFactory();
            myIS = myISF.OpenSession();

            IList<Categories> cat = myIS.CreateCriteria(typeof(Categories)).List<Categories>();
            var n = from co in cat
                where co.idcategoryroom.Equals(id)
                select co.namecategory;
            Categories c = new Categories();
            c.idcategoryroom = id;
            c.namecategory = n.ToString();

            using (myIS.BeginTransaction())
            {              
                myIS.Delete(c);               
                myIS.Transaction.Commit();
            }

            return View(1);
            
        }
            catch (Exception e)
            {
                
                Response.Write(e);
                return View(2);
                
            }

           
        }





    }
}
