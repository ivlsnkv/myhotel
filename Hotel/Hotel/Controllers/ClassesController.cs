﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using Hotel.Models;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;

namespace Hotel.Controllers
{
    public class ClassesController : Controller
    {
        
        public  Configuration myconf1;
        public  ISessionFactory myISF1;
        public  ISession myIS1;


        public ActionResult Index()
        {
            return View();
        }

        public JsonResult ClassesData()
        {
            myconf1 = new Configuration();
            myconf1.Configure();
            myISF1 = myconf1.BuildSessionFactory();
            myIS1 = myISF1.OpenSession();
            
            IList<Classes> cat = myIS1.CreateCriteria(typeof(Classes)).List<Classes>();           

            var result = new JsonResult()

            {

                Data = new
                {
                    page = 0,
                    total = 1,

                    records = 2,

                    rows = (
                from co in cat
                select new
                {
                    id = co.idclassroom,
                    cell = new string[]
                    {
                        co.idclassroom.ToString(),
                        co.nameclass
                    }

                }).ToArray()

                }

            };
            
            return result;
           
        }



    }
}
