﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Hotel.Models;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;


namespace Hotel.Controllers
{
    public class BookingController : Controller
    {
      
        public Clients t1 = new Clients();
        public string idcl;
        public Configuration myconf1;
        public ISessionFactory myISF1;
        public ISession myIS1;

        public Configuration myconf2;
        public ISessionFactory myISF2;
        public ISession myIS2;

        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult SimpleObject(FormCollection form)
        {

            int idcl = 0;
            Clients cl=new Clients();
          
            cl.lastname = form["lastname"].ToString();
            cl.firstname=form["firstname"].ToString();
            cl.patronomic = form["patronomic"].ToString();
            cl.passport = form["passport"].ToString();
            cl.city = form["city"].ToString();
            cl.address = form["address"].ToString();
            cl.phone = form["phone"].ToString();
            cl.email = form["email"].ToString();

            int arm = Convert.ToInt32(form["arm"].ToString());
            int numberguests = Convert.ToInt32(form["numberguests"].ToString());

            var arrival = Convert.ToDateTime(form["arrivaldate"].ToString());
            var departure = Convert.ToDateTime(form["departuredate"].ToString());

            myconf1 = new Configuration();
            myconf1.Configure();
            myISF1 = myconf1.BuildSessionFactory();
            myIS1 = myISF1.OpenSession();


            using (myIS1.BeginTransaction())
            {
                myIS1.Save(cl);
                idcl = cl.idclient;
                myIS1.Transaction.Commit();
            }

            return RedirectToAction("SaveBook", "Help", new { id = idcl, rm = arm, numberguests = numberguests,arrival=arrival,departure=departure }); 
        }  


    }
}
