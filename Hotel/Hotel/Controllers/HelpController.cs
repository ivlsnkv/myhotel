﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Hotel.Models;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;


namespace Hotel.Controllers
{
    public class HelpController : Controller
    {       

        public Configuration myconf2;
        public ISessionFactory myISF2;
        public ISession myIS2;




        public ActionResult Index(/*int id, int rm, int numberguests*/)
        {     

            return View();
           
        }
        public ActionResult SaveBook(int id, int rm, int numberguests, DateTime arrival, DateTime departure)
        {
            
            Booking b = new Booking();
            b.idclient = id;
            b.numberguests = numberguests;
            b.idcategoryroom = rm;
            b.arrivaldate = arrival;
            b.departuredate = departure;

            myconf2 = new Configuration();
            myconf2.Configure();
            myISF2 = myconf2.BuildSessionFactory();
            myIS2 = myISF2.OpenSession();

            using (myIS2.BeginTransaction())
            {
                myIS2.Save(b);
                myIS2.Transaction.Commit();
            }



            return View();
            
        }    


    }
}
