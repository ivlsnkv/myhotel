﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using Hotel.Models;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;


namespace Hotel.Controllers
{
    [HandleError]
    public class HomeController : Controller
    {

        public  Configuration myconf;
        public  ISessionFactory myISF;
        public  ISession myIS;

        public JsonResult GetItem()
        {
            Categories i = new Categories();
            i.idcategoryroom = 1;
            i.namecategory = "double";

            return Json(i, JsonRequestBehavior.AllowGet);
        }


        public ActionResult Index()
        {
            ViewData["Message"] = "Добро пожаловать!";

            return View();
        }

        public ActionResult About()
        {
            return View();
        }
        public ActionResult Classes()
        {
            return View();
        }       
     

        public ActionResult Rooms()
        {
            return View();
        }

        public ActionResult Categories()
        {
            return View();
        }



        public JsonResult EmployeeData()
        {
            myconf = new Configuration();
            myconf.Configure();
            myISF = myconf.BuildSessionFactory();
            myIS = myISF.OpenSession();


            List<Categories> cat = (from p in myIS.Query<Categories>() select p).ToList();
          

            var result = new// JsonResult()

            {

                Data = new { page = 0, total =1,
                    
                    records = 6, 
                    
                    rows = (
                from co in cat
                select new
                {
                    id = co.idcategoryroom.ToString().Trim(),
                    cell = new string[]
                    {
                        co.idcategoryroom.ToString().Trim(),
                        co.namecategory.Trim()
                    }

                }).ToArray()                            
                
                }

            };
                        
            return Json(result, JsonRequestBehavior.AllowGet);          
            
        }

    }
}
