﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Bytecode;
using System.Data.SqlClient;
using Hotel.Models;
using NHibernate.Mapping.ByCode;
using Hotel.Controllers;
using NHibernate.Dialect;
using System.Reflection;
using NHibernate.Cfg.MappingSchema;
using NHibernate.Tool.hbm2ddl;
using NHibernate.Linq;
using Newtonsoft.Json;

namespace Hotel.Controllers
{
    public class TypesRoomsController : Controller
    {
        public Configuration myconf1;
        public ISessionFactory myISF1;
        public ISession myIS1;



        public ActionResult Index()
        {
            return View();
        }

        public JsonResult TypesRoomsData()
        {
            myconf1 = new Configuration();
            myconf1.Configure();
            myISF1 = myconf1.BuildSessionFactory();
            myIS1 = myISF1.OpenSession();

            IList<TypesRooms> cat = myIS1.CreateCriteria(typeof(TypesRooms)).List<TypesRooms>();

            var result = new 

            { 

                    rows = (
                from co in cat
                select new
                {
                    id = co.idtyperoom,//.ToString().Trim(),
                    cell = new string[]
                    {
                        co.idtyperoom.ToString(),//.Trim(),
                        co.idcategoryroom.ToString(),
                        co.idclassroom.ToString(),
                        co.costnight.ToString(),//.Trim()
                        co.picture,
                        co.description
                    }

                }).ToArray()             

            };           

            
            return Json(result, JsonRequestBehavior.AllowGet);

        }



    }
}
