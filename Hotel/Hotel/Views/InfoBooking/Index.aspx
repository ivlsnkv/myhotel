﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Клиенты</h2>
            <table id='book'></table><div id="pager" style="text-align:center;"></div>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#book').jqGrid({
                    url: '../../InfoBooking/InfoBooking',
                    datatype: "json",
                    mtype: 'POST',
                    colNames: ['idclient', 'lastname', 'firstname', 'patronomic', 'passport', 'city', 'address', 'phone', 'email', 'namecategory', 'nameclass', 'numberguests'],
                    colModel: [
        { name: 'idclient', index: 'idclient', width: 20, stype: 'text' },
        { name: 'lastname', index: 'lastname', width: 100, sortable: true },
        { name: 'firstname', index: 'firstname', width: 70, sortable: true },
        { name: 'patronomic', index: 'patronomic', width: 150, sortable: true },
        { name: 'passport', index: 'passport', width: 70, sortable: true },
        { name: 'city', index: 'city', width: 70, sortable: true },
        { name: 'address', index: 'address', width: 150, sortable: true },
        { name: 'phone', index: 'phone', width: 70, sortable: true },
        { name: 'email', index: 'email', width: 100, sortable: true },
        { name: 'namecategory', index: 'idcategoryroom', width: 150, sortable: true },
        { name: 'nameclass', index: 'idclassroom', width: 20, sortable: true },
        { name: 'numberguests', index: 'numberguests', width: 20, sortable: true }
        

        ],
                    // rowNum: 6, // число отображаемых строк
                    //width: '1600',
                    loadonce: true, // загрузка только один раз
                    sortname: 'idclient', // сортировка по умолчанию по столбцу Id
                    sortorder: "desc", // порядок сортировки
                    caption: "  ",
                    pager:pager
                }).navGrid('#pager', { edit: false, add: false, del: false, search: true, refresh: true }); 
            });

</script> 
</asp:Content>
