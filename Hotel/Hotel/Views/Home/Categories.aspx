﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<script runat="server">
    

    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }

    protected void Page_Load(object sender, EventArgs e)
    {

       
    }
</script>
<asp:Content ID="aboutTitle" ContentPlaceHolderID="TitleContent" runat="server">
    Категории номеров
</asp:Content>

<asp:Content ID="aboutContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Редактирование категорий номеров</h2>

    
    <table id='Categories'></table><div id="pager" style="text-align:center;"></div>
        <script type="text/javascript">

    $(document).ready(function () {
        $('#Categories').jqGrid({
            url: '/Home/EmployeeData',
            datatype: "json",
            mtype: 'GET',
            jsonReader: {
                page: "page",
                total: "total",
                records: "records",
                root: "rows",
                repeatitems: false,
                id: ""
            },
             colNames: ['idcategoryroom', 'namecategory'],
            colModel: [
        { name: 'idcategoryroom', index: 'idcategoryroom', width: 30, stype: 'text' },
        { name: 'namecategory', index: 'namecategory', width: 150, sortable: true }

        ],
            
        
            rowNum: 6, // число отображаемых строк
            loadonce: true, // загрузка только один раз
            sortname: 'idcategoryroom', // сортировка по умолчанию по столбцу Id
            sortorder: "desc", // порядок сортировки
            caption: "Список Книг"
        });
    });

</script> 

    
    
</asp:Content>
