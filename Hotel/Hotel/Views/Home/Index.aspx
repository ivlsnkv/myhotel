﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage" %>

<script runat="server">

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        
    }
</script>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Домашняя страница
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: ViewData["Message"] %></h2>
    <br/><br/>
    
    <script type="text/javascript" src="../../Scripts/jquery.min.js"></script>

     <script type="text/javascript">

         $(function () {
             $('#demo_box').popmenu({
                 'controller': true,       // use control button or not
                 'width': '300px',         // width of menu
                 'background': '#34495e',  // background color of menu
                 'focusColor': '#1abc9c',  // hover color of menu's buttons
                 'borderRadius': '10px',   // radian of angles, '0' for right angle
                 'top': '50',              // pixels that move up
                 'left': '0',              // pixels that move left
                 'iconSize': '100px'       // size of menu's buttons
             });
         });
   


    </script>
    <div id="demo_box"></div>
<!--  Menu -->
        <nav class="pushy pushy-left">
            <ul>
                <li class="pushy-submenu">
                    <a href="#">Бронирование</a>
                    <ul>
                        <li class="pushy-link"><a href="/TypesRooms">Типы комнат</a></li>
                        <li class="pushy-link"><a href="/InfoBooking">Клиенты</a></li>                        
                    </ul>
                </li>                
                <li class="pushy-link"><a href="/Categories">Категории</a></li>
                <li class="pushy-link"><a href="/Classes">Классы</a></li>                
            </ul>
        </nav>

        <div class="site-overlay"></div>


        <div id="container">
       
            <div class="menu-btn"><font size="5" color="red" face="Arial">&#9776; Menu</font></div>
</div>






        <!--
<div>

        &nbsp;<asp:Menu ID="Menu1" runat="server" Orientation="Horizontal" 
        onmenuitemclick="Menu1_MenuItemClick">
        <Items>
            <asp:MenuItem Text="Номера" Value="Номера">
                <asp:MenuItem Text="Бронирование" Value="Бронирование" ></asp:MenuItem>
                <asp:MenuItem Text="Вселение" Value="Вселение"></asp:MenuItem>
                <asp:MenuItem Text="Инфо о брони определенного номера" 
                    Value="Инфо о брони определенного номера"></asp:MenuItem>
                <asp:MenuItem Text="Отмененные брони" Value="Отмененные брони"></asp:MenuItem>
                <asp:MenuItem Text="Инфо о всех бронях" Value="Инфо о всех бронях">
                </asp:MenuItem>
                <asp:MenuItem Text="Инфо о всех вселениях" Value="Инфо о всех вселениях">
                </asp:MenuItem>
                <asp:MenuItem Text="Редактирование" Value="Редактирование">
                    <asp:MenuItem Text="Цен" Value="Цен"></asp:MenuItem>
                    <asp:MenuItem Text="Номеров" Value="Номеров" NavigateUrl="~/Home/Rooms/"></asp:MenuItem>
                    <asp:MenuItem Text="Категорий" Value="Категорий" NavigateUrl="/Categories"></asp:MenuItem>
                    <asp:MenuItem Text="Классов" Value="Классов" NavigateUrl="/Classes"></asp:MenuItem>
                    <asp:MenuItem Text="Брони" Value="Брони"></asp:MenuItem>
                    <asp:MenuItem Text="Типов размещения" Value="Типов размещения" NavigateUrl="/TypesRooms/"></asp:MenuItem>
                </asp:MenuItem>
            </asp:MenuItem>
        </Items>
    </asp:Menu>
      </div>-->




          


</asp:Content>
