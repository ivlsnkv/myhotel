﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Редактирование классов номеров</h2>

    
    <table id='Classes'><tr><td></td></tr></table><div id="pager" style="text-align:center;"></div><div id="pager2"></div>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#Classes').jqGrid({
                    url: '/Classes/ClassesData',
                    datatype: "json",
                    mtype: 'POST',

                    colNames: ['idclassroom', 'nameclass'],
                    colModel: [
        { name: 'idclassroom', index: 'idclassroom', stype: 'text', width: 75,
            key: true,
            editable: true,
            editrules: { required: true}
        },
        { name: 'nameclass', index: 'nameclass', width: 150, sortable: true, width: 140,
            editable: true
        }

        ],
                    
                    rowNum: 2, // число отображаемых строк
                    //loadonce: true, // загрузка только один раз
                    sortname: 'idclassroom', // сортировка по умолчанию по столбцу Id
                    sortorder: "desc", // порядок сортировки
                    caption: "  ",
                    viewrecords: true,
                    gridview: true,
                    pager: '#pager'

                });
            }).navGrid('#pager', { edit: false, add: false, del: false, search: true, refresh: true, view: true });


</script> 

</asp:Content>
