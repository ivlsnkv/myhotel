﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Просмотр комнат</h2>


    
    

    <div id="All1"></div><div id="All2"></div>

    <script type="text/javascript">

        $.ajax({
            url: '/TypesRooms/TypesRoomsData',
            dataType: 'json',
            traditional: true,
            mtype: 'POST',
            jsonReader: {
                page: "page",
                total: "total",
                records: "records",
                root: "rows",
                repeatitems: false,
                id: ""
            },

            success: function (data) {

                for (var i = 0; i < data.rows.length; i++) {

                             //надо передать номер типа комнаты на следующую страницу
                    document.getElementById('All' + (i+1)).innerHTML = "<div title='pict" + (i+1) + "'  id='pict" + (i+1) + "'><img src='" + data.rows[i].cell[4] + "'/><a href='Booking?arm="+(i+1)+"'>Забронировать</a></div><div id='cost" + (i+1) + "'>Стоимость: " + data.rows[i].cell[3]+"</div><div id='desc" + (i+1) + "'>Описание номера: " + data.rows[i].cell[5]+"</div>";

                }

            }
        });


</script> 

</asp:Content>
