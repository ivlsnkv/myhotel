﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	Index
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../../Scripts/src/grid.loader.js" type="text/javascript"></script>
    <h2>Редактирование категорий номеров</h2>
   
    
    <table id='Categories'></table><div id="pager" class="scroll" style="text-align: center">
  </div><div id="pager2" class="scroll" style="text-align: center">
  </div>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#Categories').jqGrid({
                    url: '../../Categories/EmployeeData',
                    datatype: "json",
                    mtype: 'POST',
                    colNames: ['idcategoryroom', 'namecategory'],
                    colModel: [
        { name: 'idcategoryroom', index: 'idcategoryroom', width: 30, stype: 'text' },
        { name: 'namecategory', index: 'namecategory', width: 150, sortable: true, editable: true, editoptions: { dataUrl: "../../Categories/DeleteUser"} }

        ],

                    rowNum: 9, // число отображаемых строк
                    loadonce: true, // загрузка только один раз
                    sortname: 'idcategoryroom', // сортировка по умолчанию по столбцу Id
                    sortorder: "desc", // порядок сортировки
                    caption: "  ",
                    pager: '#pager',
                    width: '600',
                    editurl: "../../Categories/DeleteUser",
                    ondblClickRow: function (rowid) {
                        $(this).jqGrid("editGridRow", rowid);
                    },
                    onSelectRow: function (id) {
                        var data = jQuery('#Categories').getRowData(id);
                        if (data.editable == true) { 
                            jQuery('#Categories').editRow(id, true);
                        }
                    }



                }).navGrid('#pager', { edit: false, add: false, del: false, search: true, refresh: true, view: true },

                    {  
                    closeOnEscape: true, 
                    reloadAfterSubmit: true,
                    drag: true,
                    afterSubmit: function (response, postdata) {
                        if (response.responseText == "") {
                            $(this).jqGrid('setGridParam',
             { datatype: 'json' }).trigger('reloadGrid'); 
                            return [true, '']
                        }
                        else {
                            $(this).jqGrid('setGridParam',
             { datatype: 'json' }).trigger('reloadGrid'); 
                            return [false, response.responseText]                            
                        }
                    },
                    editData: {
                        EmpId: function () {
                            var sel_id = $('#Categories').jqGrid('getGridParam', 'selrow');
                            var value = $('#Categories').jqGrid('getCell', sel_id, '_id');
                            return value;
                        }
                    }
                },
{
    closeAfterAdd: true, 
    afterSubmit: function (response, postdata) {
        if (response.responseText == "") {
            $(this).jqGrid('setGridParam',
             { datatype: 'json' }).trigger('reloadGrid')
            return [true, '']
        }
        else {
            $(this).jqGrid('setGridParam',
             { datatype: 'json' }).trigger('reloadGrid')
            return [false, response.responseText]
        }
    }
},
{  
    closeOnEscape: true,
    closeAfterDelete: true,
    reloadAfterSubmit: true,
    closeOnEscape: true,
    drag: true,
    afterSubmit: function (response, postdata) {
        if (response.responseText == "") {
            $("#Categories").trigger("reloadGrid", [{ current: true}]);
            return [false, response.responseText]
        }
        else {
            $(this).jqGrid('setGridParam', { datatype: 'json' }).trigger('reloadGrid')
            return [true, response.responseText]
        }
    },
    delData: {
        EmpId: function () {
            var sel_id = $('#Categories').jqGrid('getGridParam', 'selrow');
            var value = $('#Categories').jqGrid('getCell', sel_id, '_id');
            return value;
        }
    }
},
{
    closeOnEscape: true
}

                );

            });

</script> 

</asp:Content>
